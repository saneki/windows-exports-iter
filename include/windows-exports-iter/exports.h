#ifndef __EXPORTS_H__
#define __EXPORTS_H__

#include <windows.h>
#include <psapi.h>

enum PE_TYPE {
    NT_MAGIC_32 = IMAGE_NT_OPTIONAL_HDR32_MAGIC, // 0x10b
    NT_MAGIC_64 = IMAGE_NT_OPTIONAL_HDR64_MAGIC, // 0x20b
};

const char *EXPORTS_GetErrString(int error);
int EXPORTS_GetExportDirectory(const MODULEINFO *module_info, IMAGE_EXPORT_DIRECTORY **exports);
int EXPORTS_GetNtHeaders(const MODULEINFO *module_info, void **out_headers, WORD *out_type);
int EXPORTS_ResolveDataDirectory(const MODULEINFO* module_info, IMAGE_DATA_DIRECTORY entry, void** resolved);
int EXPORTS_ResolveExportByName(const MODULEINFO* module_info, const IMAGE_EXPORT_DIRECTORY* exports, const char *name, void **resolved);
int EXPORTS_ResolveExportRVAByName(const MODULEINFO* module_info, const IMAGE_EXPORT_DIRECTORY* exports, const char *name, DWORD *resolved);
int EXPORTS_ResolveExportDirectory_32(const MODULEINFO* module_info, const IMAGE_NT_HEADERS32* nt_headers, IMAGE_EXPORT_DIRECTORY** exports);
int EXPORTS_ResolveExportDirectory_64(const MODULEINFO* module_info, const IMAGE_NT_HEADERS64* nt_headers, IMAGE_EXPORT_DIRECTORY** exports);
int EXPORTS_ValidateExportDirectory(const MODULEINFO* module_info, const IMAGE_EXPORT_DIRECTORY* exports);

typedef struct ExportsProcCtxt {
    DWORD export_directory_rva;
    IMAGE_EXPORT_DIRECTORY export_directory;
} ExportsProcCtxt;

int EXPORTS_Proc_GetExportRVA(HANDLE hProcess, const MODULEINFO *module_info, ExportsProcCtxt *ctxt, WORD *out_type);
int EXPORTS_Proc_ResolveExportByName(HANDLE hProcess, const MODULEINFO *module_info, const ExportsProcCtxt *ctxt, const char *name, void **resolved);
int EXPORTS_Proc_ResolveExportRVAByName(HANDLE hProcess, const MODULEINFO *module_info, const ExportsProcCtxt *ctxt, const char *name, DWORD *resolved);

#endif // __EXPORTS_H__
