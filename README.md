# Windows Exports Iter

This is some code written to resolve exported symbols at runtime. It provides two distinct interfaces for resolving exports from different sources:

- From a module within the same address space (process).
- From a module within a different process via `ReadProcessMemory`.

These interfaces are not entirely parallel due to the extra performance costs of reading process memory.

The primary intent for writing this code was to add proper [DuckStation] support for [Mouse Injector], as its game `RAM` address is exported by the binary.

[DuckStation]:https://www.duckstation.org/
[Mouse Injector]:https://github.com/garungorp/MouseInjectorDolphinDuck
