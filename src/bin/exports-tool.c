#include <stdio.h>
#include <windows.h>
#include <TlHelp32.h>
#include "windows-exports-iter/exports.h"

__declspec(dllexport) void *SampleExport = NULL;

HANDLE FindProcessByExe(const char *exe) {
    HANDLE hProcess = NULL;
    HANDLE processes = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (processes == INVALID_HANDLE_VALUE) {
        return NULL;
    }
    PROCESSENTRY32 process_entry;
    if (!Process32First(processes, &process_entry)) {
        CloseHandle(processes);
        return NULL;
    }
    do {
        if (strcmp(process_entry.szExeFile, exe) == 0) {
            hProcess = OpenProcess(
                PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_QUERY_INFORMATION,
                FALSE,
                process_entry.th32ProcessID);
            break;
        }
    } while (Process32Next(processes, &process_entry));
    CloseHandle(processes);
    return hProcess;
}

BOOL GetRootModuleInfo(HANDLE hProcess, MODULEINFO *module_info) {
    HMODULE modules[1];
	DWORD cbNeeded;
	if (EnumProcessModules(hProcess, modules, sizeof(modules), &cbNeeded) == 0) {
		fprintf(stderr, "Unable to enumerate process modules.\n");
		return FALSE;
	}
    if (GetModuleInformation(hProcess, modules[0], module_info, sizeof(*module_info)) == 0) {
		fprintf(stderr, "Unable to get module information.\n");
        return FALSE;
    }
    return TRUE;
}

void* PerformSelfProcessResolveTest(const char *symbol) {
    HANDLE hProcess = GetCurrentProcess();

    MODULEINFO module_info;
    if (!GetRootModuleInfo(hProcess, &module_info)) {
        return NULL;
    }

    int ret;
    IMAGE_EXPORT_DIRECTORY *export_directory;
    if ((ret = EXPORTS_GetExportDirectory(&module_info, &export_directory)) != 0) {
        fprintf(stderr, "ERROR: %s\n", EXPORTS_GetErrString(ret));
        return NULL;
    }

    void *resolved;
    if ((ret = EXPORTS_ResolveExportByName(&module_info, export_directory, symbol, &resolved)) != 0) {
        fprintf(stderr, "ERROR: %s\n", EXPORTS_GetErrString(ret));
        return NULL;
    }

    return resolved;
}

void* PerformInterProcessResolveTest(const char *exe, const char *symbol) {
    HANDLE hProcess = FindProcessByExe(exe);
    if (hProcess == NULL) {
        fprintf(stderr, "Unable to find process: %s\n", exe);
        return NULL;
    }

    MODULEINFO module_info;
    if (!GetRootModuleInfo(hProcess, &module_info)) {
        return NULL;
    }

    int ret;

    ExportsProcCtxt ctxt;
    WORD pe_type;
    if ((ret = EXPORTS_Proc_GetExportRVA(hProcess, &module_info, &ctxt, &pe_type)) != 0) {
        fprintf(stderr, "ERROR: %s\n", EXPORTS_GetErrString(ret));
    }

    if (pe_type != NT_MAGIC_64) {
		fprintf(stderr, "Unsupported PE type value: %04x\n", pe_type);
		return NULL;
	}

	void *resolved = NULL;
	if ((ret = EXPORTS_Proc_ResolveExportByName(hProcess, &module_info, &ctxt, symbol, &resolved)) != 0) {
		fprintf(stderr, "ERROR: %s\n", EXPORTS_GetErrString(ret));
		return NULL;
	}

    return resolved;
}

void PrintCmdUsageProc(const char *path) {
    fprintf(stderr, "usage: %s proc <exe-name> <symbol-name>\n", path);
}

void PrintCmdUsageSelf(const char *path) {
    fprintf(stderr, "usage: %s self <symbol-name>\n", path);
}

int main(int argc, char **argv) {
    if (argc < 2) {
        PrintCmdUsageProc(argv[0]);
        PrintCmdUsageSelf(argv[0]);
        return 1;
    }

    if (strcmp(argv[1], "proc") == 0) {
        if (argc < 4) {
            PrintCmdUsageProc(argv[0]);
            return 1;
        }
        const char *exe = argv[2];
        const char *symbol = argv[3];
        void *resolved = PerformInterProcessResolveTest(exe, symbol);
        if (resolved != NULL) {
            printf("Resolved: %p\n", resolved);
        }
    } else if (strcmp(argv[1], "self") == 0) {
        if (argc < 3) {
            PrintCmdUsageSelf(argv[0]);
            return 1;
        }
        const char *symbol = argv[2];
        void *resolved = PerformSelfProcessResolveTest(symbol);
        if (resolved != NULL) {
            printf("Resolved: %p\n", resolved);
        }
    } else {
        fprintf(stderr, "Unknown command: %s\n", argv[1]);
        return 1;
    }

    return 0;
}
