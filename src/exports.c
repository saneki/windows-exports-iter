#include <stdint.h>
#include <windows.h>
#include <psapi.h>
#include "windows-exports-iter/exports.h"

static BOOL ReadProcBytes(HANDLE hProcess, LPCVOID lpBaseAddress, uint8_t *bytes, size_t length) {
    return ReadProcessMemory(hProcess, lpBaseAddress, (LPVOID)bytes, length, NULL);
}

static BOOL ReadProcU16(HANDLE hProcess, LPCVOID lpBaseAddress, WORD *value) {
    return ReadProcessMemory(hProcess, lpBaseAddress, (LPVOID)value, sizeof(*value), NULL);
}

static BOOL ReadProcU32(HANDLE hProcess, LPCVOID lpBaseAddress, DWORD *value) {
    return ReadProcessMemory(hProcess, lpBaseAddress, (LPVOID)value, sizeof(*value), NULL);
}

static BOOL ReadProcU32Array(HANDLE hProcess, LPCVOID lpBaseAddress, DWORD *array, size_t length) {
    return ReadProcessMemory(hProcess, lpBaseAddress, (LPVOID)array, sizeof(*array) * length, NULL);
}

enum ExportsError {
    EXPORTS_ERR_NONE = 0,
    // Reading DOS/NT headers, find export directory:
    EXPORTS_ERR_BAD_DOS_HEADER,
    EXPORTS_ERR_BAD_DOS_SIG,
    EXPORTS_ERR_BAD_OPTIONAL_HEADER,
    EXPORTS_ERR_DATA_DIRECTORY_DOES_NOT_EXIST,
    EXPORTS_ERR_BAD_DATA_DIRECTORY,
    EXPORTS_ERR_BAD_NT_HEADERS32,
    EXPORTS_ERR_BAD_NT_HEADERS64,
    EXPORTS_ERR_BAD_NT_HEADERS_SIG,
    EXPORTS_ERR_UNKNOWN_PE_TYPE,
    // Validating export directory:
    EXPORTS_ERR_BAD_FUNCTIONS_SECTION,
    EXPORTS_ERR_BAD_NAMES_SECTION,
    EXPORTS_ERR_BAD_NAME_ORDINALS_SECTION,
    // Iterating exports:
    EXPORTS_ERR_EXPORT_NAME_NOT_FOUND,
    EXPORTS_ERR_EXPORT_NAME_OOB,
    EXPORTS_ERR_EXPORT_NAME_ORDINAL_TO_FUNCTION_OOB,
    EXPORTS_ERR_EXPORT_RVA_OOB,
    // Proc reading export directory:
    EXPORTS_ERR_PROC_READ_DOS_SIG,
    EXPORTS_ERR_PROC_READ_NT_HEADERS_RVA,
    EXPORTS_ERR_PROC_READ_PE_TYPE,
    EXPORTS_ERR_PROC_READ_NT_HEADERS_SIG,
    EXPORTS_ERR_PROC_READ_EXPORT_DATA_DIRECTORY,
    EXPORTS_ERR_PROC_READ_EXPORT_DIRECTORY,
    // Proc reading while iterating exports:
    EXPORTS_ERR_PROC_READ_EXPORT_NAME_RVA,
    EXPORTS_ERR_PROC_READ_EXPORT_NAME,
    EXPORTS_ERR_PROC_READ_EXPORT_NAME_ORDINAL,
    EXPORTS_ERR_PROC_READ_EXPORT_VALUE_RVA,
};

const char *EXPORTS_GetErrString(int error) {
    switch (error) {
        case EXPORTS_ERR_NONE:
            return "None";
        case EXPORTS_ERR_BAD_DOS_HEADER:
            return "DOS header was not recognized.";
        case EXPORTS_ERR_BAD_DOS_SIG:
            return "DOS signature was not recognized.";
        case EXPORTS_ERR_BAD_OPTIONAL_HEADER:
            return "Optional Header was malformed.";
        case EXPORTS_ERR_DATA_DIRECTORY_DOES_NOT_EXIST:
            return "Data directory does not exist.";
        case EXPORTS_ERR_BAD_DATA_DIRECTORY:
            return "Data directory was malformed.";
        case EXPORTS_ERR_BAD_NT_HEADERS32:
            return "IMAGE_NT_HEADERS32 was malformed.";
        case EXPORTS_ERR_BAD_NT_HEADERS64:
            return "IMAGE_NT_HEADERS64 was malformed.";
        case EXPORTS_ERR_BAD_NT_HEADERS_SIG:
            return "NT Headers signature was not recognized.";
        case EXPORTS_ERR_UNKNOWN_PE_TYPE:
            return "Unknown PE type.";
        case EXPORTS_ERR_BAD_FUNCTIONS_SECTION:
            return "Export functions section was malformed.";
        case EXPORTS_ERR_BAD_NAMES_SECTION:
            return "Exports names section was malformed.";
        case EXPORTS_ERR_BAD_NAME_ORDINALS_SECTION:
            return "Exports name ordinals section was malformed.";
        case EXPORTS_ERR_EXPORT_NAME_NOT_FOUND:
            return "Export symbol name was not found.";
        case EXPORTS_ERR_EXPORT_NAME_OOB:
            return "Export symbol name RVA was out-of-bounds.";
        case EXPORTS_ERR_EXPORT_NAME_ORDINAL_TO_FUNCTION_OOB:
            return "Export symbol name ordinal out-of-bounds for function list.";
        case EXPORTS_ERR_EXPORT_RVA_OOB:
            return "Export resolved RVA was out-of-bounds.";
        case EXPORTS_ERR_PROC_READ_DOS_SIG:
            return "Process: Unable to read DOS signature.";
        case EXPORTS_ERR_PROC_READ_NT_HEADERS_RVA:
            return "Process: Unable to read NT Headers RVA.";
        case EXPORTS_ERR_PROC_READ_PE_TYPE:
            return "Process: Unable to read PE type.";
        case EXPORTS_ERR_PROC_READ_NT_HEADERS_SIG:
            return "Process: Unable to read NT Headers signature.";
        case EXPORTS_ERR_PROC_READ_EXPORT_DATA_DIRECTORY:
            return "Process: Unable to read export data directory.";
        case EXPORTS_ERR_PROC_READ_EXPORT_DIRECTORY:
            return "Process: Unable to read export directory.";
        case EXPORTS_ERR_PROC_READ_EXPORT_NAME_RVA:
            return "Process: Unable to read export name RVA.";
        case EXPORTS_ERR_PROC_READ_EXPORT_NAME:
            return "Process: Unable to read export name.";
        case EXPORTS_ERR_PROC_READ_EXPORT_NAME_ORDINAL:
            return "Process: Unable to read export name ordinal.";
        case EXPORTS_ERR_PROC_READ_EXPORT_VALUE_RVA:
            return "Process: Unable to read export value RVA.";
        default:
            return "Unknown error value";
    }
}

static BOOL CheckOverflowAndOutOfBounds(const MODULEINFO* module_info, DWORD base_rva, DWORD length) {
    if (base_rva + length < base_rva) {
        return TRUE;
    }
    if (module_info->SizeOfImage < base_rva + length) {
        return TRUE;
    }
    return FALSE;
}

int EXPORTS_ValidateExportDirectory(const MODULEINFO* module_info, const IMAGE_EXPORT_DIRECTORY* exports) {
    if (CheckOverflowAndOutOfBounds(module_info, exports->AddressOfFunctions, exports->NumberOfFunctions * sizeof(DWORD))) {
        return EXPORTS_ERR_BAD_FUNCTIONS_SECTION;
    }
    if (CheckOverflowAndOutOfBounds(module_info, exports->AddressOfNames, exports->NumberOfNames * sizeof(DWORD))) {
        return EXPORTS_ERR_BAD_NAMES_SECTION;
    }
    if (CheckOverflowAndOutOfBounds(module_info, exports->AddressOfNameOrdinals, exports->NumberOfNames * sizeof(WORD))) {
        return EXPORTS_ERR_BAD_NAME_ORDINALS_SECTION;
    }
    return 0;
}

int EXPORTS_ResolveExportRVAByName(const MODULEINFO* module_info, const IMAGE_EXPORT_DIRECTORY* exports, const char *name, DWORD *resolved) {
    const uint8_t* base = (const uint8_t*)module_info->lpBaseOfDll;

    int validation = EXPORTS_ValidateExportDirectory(module_info, exports);
    if (validation != 0) {
        return validation;
    }

    for (DWORD i = 0; i < exports->NumberOfNames; i++) {
        DWORD export_name_rva = *(const DWORD*)(base + exports->AddressOfNames + (i * sizeof(DWORD)));

        // Check: Start of export name is within linear image.
        if (module_info->SizeOfImage <= export_name_rva) {
            return EXPORTS_ERR_EXPORT_NAME_OOB;
        }

        const char *export_name = (const char*)(base + export_name_rva);
        if (strcmp(name, export_name) != 0) {
            continue;
        }

        WORD export_name_ordinal = *(const WORD*)(base + exports->AddressOfNameOrdinals + (i * sizeof(WORD)));
        if (exports->NumberOfFunctions <= export_name_ordinal) {
            return EXPORTS_ERR_EXPORT_NAME_ORDINAL_TO_FUNCTION_OOB;
        }

        *resolved = *(const DWORD*)(base + exports->AddressOfFunctions + (export_name_ordinal * sizeof(DWORD)));
        return 0;
    }

    return EXPORTS_ERR_EXPORT_NAME_NOT_FOUND;
}

// TODO: Pointer into guest memory should either be typed for the address space (32-bits or 64-bits distinctly), or always be 64-bits.
int EXPORTS_ResolveExportByName(const MODULEINFO* module_info, const IMAGE_EXPORT_DIRECTORY* exports, const char *name, void **resolved) {
    DWORD rva;
    int ret = EXPORTS_ResolveExportRVAByName(module_info, exports, name, &rva);
    if (ret != 0) {
        return ret;
    }
    if (module_info->SizeOfImage <= rva) {
        return EXPORTS_ERR_EXPORT_RVA_OOB;
    }
    *resolved = (void*)(((uint8_t*)module_info->lpBaseOfDll) + rva);
    return 0;
}

int EXPORTS_ResolveDataDirectory(const MODULEINFO* module_info, IMAGE_DATA_DIRECTORY entry, void** resolved) {
    const DWORD dir_rva = entry.VirtualAddress;
    const DWORD dir_size = entry.Size;

    if (dir_rva == 0) {
        return EXPORTS_ERR_DATA_DIRECTORY_DOES_NOT_EXIST;
    }

    // Overflow check for Directory data end RVA.
    if ((dir_rva + dir_size) < dir_rva) {
        return EXPORTS_ERR_BAD_DATA_DIRECTORY;
    }
    // Check: Directory data is within linear image.
    if (module_info->SizeOfImage < (dir_rva + dir_size)) {
        return EXPORTS_ERR_BAD_DATA_DIRECTORY;
    }

    *resolved = (void*)((uint8_t*)module_info->lpBaseOfDll + dir_rva);
    return 0;
}

int EXPORTS_ResolveExportDirectory_32(const MODULEINFO* module_info, const IMAGE_NT_HEADERS32* nt_headers, IMAGE_EXPORT_DIRECTORY** exports) {
    void *resolved = NULL;
    int ret = EXPORTS_ResolveDataDirectory(module_info, nt_headers->OptionalHeader.DataDirectory[0], &resolved);
    if (ret == 0) {
        *exports = (IMAGE_EXPORT_DIRECTORY*)resolved;
    }
    return ret;
}

int EXPORTS_ResolveExportDirectory_64(const MODULEINFO* module_info, const IMAGE_NT_HEADERS64* nt_headers, IMAGE_EXPORT_DIRECTORY** exports) {
    void *resolved = NULL;
    int ret = EXPORTS_ResolveDataDirectory(module_info, nt_headers->OptionalHeader.DataDirectory[0], &resolved);
    if (ret == 0) {
        *exports = (IMAGE_EXPORT_DIRECTORY*)resolved;
    }
    return ret;
}

int EXPORTS_GetExportDirectory(const MODULEINFO *module_info, IMAGE_EXPORT_DIRECTORY **exports) {
    void *nt_headers;
    WORD pe_type;
    int ret = EXPORTS_GetNtHeaders(module_info, &nt_headers, &pe_type);
    if (ret != 0) {
        return ret;
    }

    if (pe_type == NT_MAGIC_32) {
        IMAGE_NT_HEADERS32 *nt_headers32 = (IMAGE_NT_HEADERS32*)nt_headers;
        return EXPORTS_ResolveExportDirectory_32(module_info, nt_headers32, exports);
    } else if (pe_type == NT_MAGIC_64) {
        IMAGE_NT_HEADERS64 *nt_headers64 = (IMAGE_NT_HEADERS64*)nt_headers;
        return EXPORTS_ResolveExportDirectory_64(module_info, nt_headers64, exports);
    }

    return EXPORTS_ERR_UNKNOWN_PE_TYPE;
}

int EXPORTS_GetNtHeaders(const MODULEINFO *module_info, void **out_headers, WORD *out_type) {
    // Check: Can reach e_lfanew.
    if (module_info->SizeOfImage < 0x40) {
        return EXPORTS_ERR_BAD_DOS_HEADER;
    }

    unsigned char* base = module_info->lpBaseOfDll;
    if (base[0] != 'M' || base[1] != 'Z') {
        return EXPORTS_ERR_BAD_DOS_SIG;
    }

    // TODO: Check if this will be big-endian on ARM Windows? If so may need to swap.
    DWORD e_lfanew = *(DWORD*)(base + 0x3C);
    DWORD optional_magic_offset = e_lfanew + sizeof(DWORD) + sizeof(IMAGE_FILE_HEADER);

    // Overflow check for Optional Header magic start.
    if (optional_magic_offset < e_lfanew) {
        return EXPORTS_ERR_BAD_OPTIONAL_HEADER;
    }
    // Overflow check for Optional Header magic end.
    if ((optional_magic_offset + sizeof(WORD)) < optional_magic_offset) {
        return EXPORTS_ERR_BAD_OPTIONAL_HEADER;
    }
    // Optional Header magic end is within linear image.
    if (module_info->SizeOfImage < (optional_magic_offset + sizeof(WORD))) {
        return EXPORTS_ERR_BAD_OPTIONAL_HEADER;
    }

    WORD optional_magic = *(WORD*)(base + optional_magic_offset);
    *out_type = optional_magic;
    if (optional_magic == 0x10B) {
        // Handle: PE32 (32-bit address space).
        // Overflow check for e_lfanew.
        if ((e_lfanew + sizeof(IMAGE_NT_HEADERS32)) < e_lfanew) {
            return EXPORTS_ERR_BAD_NT_HEADERS32;
        }
        // NT Headers end is within linear image.
        if (module_info->SizeOfImage < (e_lfanew + sizeof(IMAGE_NT_HEADERS32))) {
            return EXPORTS_ERR_BAD_NT_HEADERS32;
        }

        IMAGE_NT_HEADERS32* nt_headers32 = (IMAGE_NT_HEADERS32*)(base + e_lfanew);
        // Check NT Headers signature.
        if (nt_headers32->Signature != 0x4550) {
            return EXPORTS_ERR_BAD_NT_HEADERS_SIG;
        }

        *out_headers = nt_headers32;
        return 0;
    } else if (optional_magic == 0x20B) {
        // Handle: PE32+ (64-bit address space).
        // Overflow check for e_lfanew.
        if ((e_lfanew + sizeof(IMAGE_NT_HEADERS64)) < e_lfanew) {
            return EXPORTS_ERR_BAD_NT_HEADERS64;
        }
        // NT Headers end is within linear image.
        if (module_info->SizeOfImage < (e_lfanew + sizeof(IMAGE_NT_HEADERS64))) {
            return EXPORTS_ERR_BAD_NT_HEADERS64;
        }

        IMAGE_NT_HEADERS64* nt_headers64 = (IMAGE_NT_HEADERS64*)(base + e_lfanew);
        // Check NT Headers signature.
        if (nt_headers64->Signature != 0x4550) {
            return EXPORTS_ERR_BAD_NT_HEADERS_SIG;
        }

        *out_headers = nt_headers64;
        return 0;
    }

    return EXPORTS_ERR_UNKNOWN_PE_TYPE;
}

int EXPORTS_Proc_GetExportRVA(HANDLE hProcess, const MODULEINFO *module_info, ExportsProcCtxt *ctxt, WORD *out_type) {
    unsigned char* base = module_info->lpBaseOfDll;

    // Check: Can reach e_lfanew.
    if (module_info->SizeOfImage < 0x40) {
        return EXPORTS_ERR_BAD_DOS_HEADER;
    }

    uint8_t dos_signature[2];
    if (!ReadProcBytes(hProcess, (LPCVOID)base, dos_signature, sizeof(dos_signature))) {
        return EXPORTS_ERR_PROC_READ_DOS_SIG;
    }
    if (dos_signature[0] != 'M' || dos_signature[1] != 'Z') {
        return EXPORTS_ERR_BAD_DOS_SIG;
    }

    // TODO: Check if this will be big-endian on ARM Windows? If so may need to swap.
    DWORD e_lfanew;
    if (!ReadProcU32(hProcess, (LPCVOID)(base + 0x3C), &e_lfanew)) {
        return EXPORTS_ERR_PROC_READ_NT_HEADERS_RVA;
    }

    DWORD optional_magic_offset = e_lfanew + sizeof(DWORD) + sizeof(IMAGE_FILE_HEADER);

    // Overflow check for Optional Header magic start.
    if (optional_magic_offset < e_lfanew) {
        return EXPORTS_ERR_BAD_OPTIONAL_HEADER;
    }
    // Overflow check for Optional Header magic end.
    if ((optional_magic_offset + sizeof(WORD)) < optional_magic_offset) {
        return EXPORTS_ERR_BAD_OPTIONAL_HEADER;
    }
    // Optional Header magic end is within linear image.
    if (module_info->SizeOfImage < (optional_magic_offset + sizeof(WORD))) {
        return EXPORTS_ERR_BAD_OPTIONAL_HEADER;
    }

    WORD optional_magic;
    if (!ReadProcU16(hProcess, (LPCVOID)(base + optional_magic_offset), &optional_magic)) {
        return EXPORTS_ERR_PROC_READ_PE_TYPE;
    }
    *out_type = optional_magic;

    if (optional_magic != 0x10B && optional_magic != 0x20B) {
        return EXPORTS_ERR_UNKNOWN_PE_TYPE;
    }

    DWORD nt_headers_signature;
    if (!ReadProcU32(hProcess, (LPCVOID)(base + e_lfanew), &nt_headers_signature)) {
        return EXPORTS_ERR_PROC_READ_NT_HEADERS_SIG;
    }

    // Check NT Headers signature.
    if (nt_headers_signature != 0x4550) {
        return EXPORTS_ERR_BAD_NT_HEADERS_SIG;
    }

    uint8_t* export_directory_entry = NULL;
    if (optional_magic == 0x10B) {
        // Handle: PE32 (32-bit address space).
        if (CheckOverflowAndOutOfBounds(module_info, e_lfanew, sizeof(IMAGE_NT_HEADERS32))) {
            return EXPORTS_ERR_BAD_NT_HEADERS32;
        }
        export_directory_entry = base + e_lfanew + sizeof(DWORD) + sizeof(IMAGE_FILE_HEADER) + 0x60;
    } else if (optional_magic == 0x20B) {
        // Handle: PE32+ (64-bit address space).
        if (CheckOverflowAndOutOfBounds(module_info, e_lfanew, sizeof(IMAGE_NT_HEADERS64))) {
            return EXPORTS_ERR_BAD_NT_HEADERS64;
        }
        export_directory_entry = base + e_lfanew + sizeof(DWORD) + sizeof(IMAGE_FILE_HEADER) + 0x70;
    } else {
        return EXPORTS_ERR_UNKNOWN_PE_TYPE;
    }

    DWORD export_directory_info[2];
    if (!ReadProcU32Array(hProcess, (LPCVOID)export_directory_entry, export_directory_info, 2)) {
        return EXPORTS_ERR_PROC_READ_EXPORT_DATA_DIRECTORY;
    }

    const DWORD export_directory_rva = export_directory_info[0];
    const DWORD export_directory_size = export_directory_info[1];
    if (CheckOverflowAndOutOfBounds(module_info, export_directory_rva, export_directory_size)) {
        return EXPORTS_ERR_BAD_DATA_DIRECTORY;
    }

    ctxt->export_directory_rva = export_directory_rva;

    if (!ReadProcBytes(hProcess, (LPCVOID)(base + export_directory_rva), (uint8_t*)&(ctxt->export_directory), sizeof(IMAGE_EXPORT_DIRECTORY))) {
        return EXPORTS_ERR_PROC_READ_EXPORT_DIRECTORY;
    }

    return 0;
}

// This is just an assumption.
#define MAX_SYMBOL_NAME 260

int EXPORTS_Proc_ResolveExportRVAByName(HANDLE hProcess, const MODULEINFO *module_info, const ExportsProcCtxt *ctxt, const char *name, DWORD *resolved) {
    const uint8_t* base = (const uint8_t*)module_info->lpBaseOfDll;
    const IMAGE_EXPORT_DIRECTORY *exports = &ctxt->export_directory;

    int validation = EXPORTS_ValidateExportDirectory(module_info, exports);
    if (validation != 0) {
        return validation;
    }

    for (DWORD i = 0; i < exports->NumberOfNames; i++) {
        DWORD export_name_rva;
        if (!ReadProcU32(hProcess, (LPCVOID)(base + exports->AddressOfNames + (i * sizeof(DWORD))), &export_name_rva)) {
            return EXPORTS_ERR_PROC_READ_EXPORT_NAME_RVA;
        }

        // Check: Start of export name is within linear image.
        if (module_info->SizeOfImage <= export_name_rva) {
            return EXPORTS_ERR_EXPORT_NAME_OOB;
        }

        char export_name_buffer[MAX_SYMBOL_NAME];
        if (!ReadProcBytes(hProcess, (LPCVOID)(base + export_name_rva), (uint8_t*)export_name_buffer, sizeof(export_name_buffer))) {
            return EXPORTS_ERR_PROC_READ_EXPORT_NAME;
        }
        export_name_buffer[(MAX_SYMBOL_NAME) - 1] = 0;

        if (strcmp(name, export_name_buffer) != 0) {
            continue;
        }

        WORD export_name_ordinal;
        if (!ReadProcU16(hProcess, (LPCVOID)(base + exports->AddressOfNameOrdinals + (i * sizeof(WORD))), &export_name_ordinal)) {
            return EXPORTS_ERR_PROC_READ_EXPORT_NAME_ORDINAL;
        }
        if (exports->NumberOfFunctions <= export_name_ordinal) {
            return EXPORTS_ERR_EXPORT_NAME_ORDINAL_TO_FUNCTION_OOB;
        }

        DWORD export_rva;
        if (!ReadProcU32(hProcess, (base + exports->AddressOfFunctions + (export_name_ordinal * sizeof(DWORD))), &export_rva)) {
            return EXPORTS_ERR_PROC_READ_EXPORT_VALUE_RVA;
        }
        *resolved = export_rva;
        return 0;
    }

    return EXPORTS_ERR_EXPORT_NAME_NOT_FOUND;
}

int EXPORTS_Proc_ResolveExportByName(HANDLE hProcess, const MODULEINFO *module_info, const ExportsProcCtxt *ctxt, const char *name, void **resolved) {
    DWORD rva;
    int ret = EXPORTS_Proc_ResolveExportRVAByName(hProcess, module_info, ctxt, name, &rva);
    if (ret != 0) {
        return ret;
    }
    if (module_info->SizeOfImage <= rva) {
        return EXPORTS_ERR_EXPORT_RVA_OOB;
    }
    *resolved = (void*)(((uint8_t*)module_info->lpBaseOfDll) + rva);
    return 0;
}
